/**bài 1 
 * input:số ngày đã làm =30
 * lương 1 ngày là 100.000
 * step1:tạo biến ngày công 
 * lương 1 ngày 
 * và tổng lương tính được 
 * 
 * out put:console kết quả lương đã tính 
 */

console.log("bài 1 tính số tiền lương ");
var ngayCong = 30;
console.log('ngaycong: ', ngayCong);
var luong1ngay = 100000;
console.log('luong1ngay: ', luong1ngay);
var luongNhanduoc = ngayCong * luong1ngay;
console.log('luongNhanduoc: ', luongNhanduoc);

/**bài 2:tính giá trị trung bình 5 số thực 
 * input: 5 số thực 
 * step:tạo 5 biến có 5 số thực 5,15,50,95,125
 *-tạo biến kết quả có tổng 5 số chia cho 5 
 * output: console giá trị trung bình được tính 
 */
console.log("bài 2 tính giá trị trung bình 5 số thực");
var num1 = 5;
var num2 = 15;
var num3 = 50;
var num4 = 95;
var num5 = 125;
console.log("các số thực:",num1,num2, num3,num4,num5);
var trungbinh = (num1+num2+num3+num4+num5) / 5;
console.log('giá trị trung bình: ', trungbinh);
/**
 * bài 3: quy đổi tiền 
 * input:số tiền cần quy đổi =4
 * step
 * -tạo biến có số tiền vnd=23.500
 * usd=4
 * -tạo biến tính số usd * vnd 
 * output:kết quả cần quy đổi 
 */
console.log("bài 3 quy đổi tiền ");
var usd = 4;
console.log('usd cần đổi: ', usd);
var vnd = 23500
console.log('giá trị 1 usd : ', vnd );
var tienDoi = usd * vnd;
console.log('giá trị quy đổi : ', tienDoi);
/**
 * bài 4:tính diện tích chu vi hình chữ nhật 
 * input:chiều dài=35 và chiều rộng=50
 * step:
 * -tạo biến chiều dài và chiều rộng
 * -tạo biến diện tích = dài nhân rộng 
 * -chu vi bằng dài cộng rộng tất cả nhân 2 
 * output:kết quả diện tích và chu vi 
 */
console.log("bài 4 tính diện tích chu vi hình chữ nhật");
var chieuRong = 50;
console.log('chiều rộng : ', chieuRong);
var chieuDai = 35;
console.log('chiều dài : ', chieuDai);
var dienTich = chieuDai * chieuRong;
console.log('Diện tích : ', dienTich);
var chuVi = (chieuDai + chieuRong) *2;
console.log('Chu vi : ', chuVi);
/**
 * bài 5 tính tổng 2 ký số 
 * input:nhập vào 2 chữ số 72
 * step
 * -tạo biến 1 ký số 
 * -tạo biến hàng đơn vị chia số lấy dư cho 10 
 * -tạo biến hàng chục chia số cho 10 
 * -tạo biến tổng ( đơn vị + hàng chục )
 * output: console kết quả tổng 
 */
console.log("bài 5 tính tổng 2 ký số ");
var number = 72;
console.log('number: ', number);
var hangChuc = Math.floor(number/10);
console.log('hàng chục : ', hangChuc);
var hangDonvi = number % 10;
console.log('hàng đơn vị : ', hangDonvi);
var tong = hangChuc + hangDonvi;
console.log('tổng : ', tong);















